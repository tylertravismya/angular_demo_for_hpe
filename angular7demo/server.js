// server.js
const express = require('express'),
	path = require('path'),
	bodyParser = require('body-parser'), 
	cors = require('cors'),
	mongoose = require('mongoose'),
    matchupRoutes = require('./src/app/expressRoutes/MatchupRoutes'),
    leagueRoutes = require('./src/app/expressRoutes/LeagueRoutes'),
    playerRoutes = require('./src/app/expressRoutes/PlayerRoutes'),
    tournamentRoutes = require('./src/app/expressRoutes/TournamentRoutes'),
    alleyRoutes = require('./src/app/expressRoutes/AlleyRoutes'),
    baseRoutes = require('./src/app/expressRoutes/BaseRoutes'),
    laneRoutes = require('./src/app/expressRoutes/LaneRoutes'),
    gameRoutes = require('./src/app/expressRoutes/GameRoutes'),
	config = require('./config/mongoDb.js');

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGO_HOST_ADDRESS || config.DB, {
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 1000
  }).then(
    () => {console.log('Database is connected') },
    err => { console.log('Can not connect to the database: ' + err)}
  );

const app = express();
app.use(bodyParser.json());
app.use(cors());
const port = 4000;

app.use('/Matchup', matchupRoutes);
app.use('/League', leagueRoutes);
app.use('/Player', playerRoutes);
app.use('/Tournament', tournamentRoutes);
app.use('/Alley', alleyRoutes);
app.use('/Base', baseRoutes);
app.use('/Lane', laneRoutes);
app.use('/Game', gameRoutes);

const server = app.listen(port, function(){
  console.log('Listening on port ' + port);
});
