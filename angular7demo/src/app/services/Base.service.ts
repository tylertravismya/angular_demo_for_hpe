import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {Base} from '../models/Base';
import { HelperBaseService } from './helperbase.service';

@Injectable()
export class BaseService extends HelperBaseService {

	//********************************************************************
	// general holder 
	//********************************************************************
	base : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {
 	    super();
    }
 	
	//********************************************************************
	// add a Base 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addBase() : Promise<any> {
    	const uri = this.ormUrl + '/Base/add';
    	const obj = {
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all Base 
	// returns the results untouched as JSON representation of an
	// array of Base models
	// delegates via URI to an ORM handler
	//********************************************************************
	getBases() {
    	const uri = this.ormUrl + '/Base';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a Base 
	// returns the results untouched as a JSON representation of a
	// Base model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editBase(id) {
    	const uri = this.ormUrl + '/Base/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a Base 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
    updateBase(id)  : Promise<any>  {
    	const uri = this.ormUrl + '/Base/update/' + id;
    	const obj = {
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a Base 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteBase(id)  : Promise<any> {
    	const uri = this.ormUrl + '/Base/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    	

	//********************************************************************
	// saveHelper - internal helper to save a Base
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = this.ormUrl + '/Base/update/' + this.base._id;		
		
    	return this
      			.http
      			.post(uri, this.base)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a Base
	//********************************************************************	
	loadHelper( id ) {
		this.editBase(id)
        		.subscribe(res => {
        			this.base = res;
      			});
	}
}