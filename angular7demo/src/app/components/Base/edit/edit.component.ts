import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseService } from '../../../services/Base.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { SubBaseComponent } from '../../Base/sub.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditBaseComponent extends SubBaseComponent implements OnInit {

  base: any;
  baseForm: FormGroup;
  title = 'Edit Base';
  
  constructor( http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: BaseService, 
  				private fb: FormBuilder) {
    super(http);
    this.createForm();
   }

  createForm() {
    this.baseForm = this.fb.group({
   });
  }
  updateBase() {
    this.route.params.subscribe(params => {
    	this.service.updateBase(params['id'])
      		.then(success => this.router.navigate(['/indexBase']) );
  });
}

// initialization
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.base = this.service.editBase(params['id']).subscribe(res => {
        this.base = res;
      });
    });
    
    super.ngOnInit();
  }
}
