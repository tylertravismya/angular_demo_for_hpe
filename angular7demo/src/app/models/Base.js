var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Base
var Base = new Schema({
},{
    collection: 'bases'
});

module.exports = mongoose.model('Base', Base);